# -*- coding: utf-8 -*-
from odoo import models, fields, api


class area_corporacion(models.Model):
    _name = 'area.area'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Área Corporación"

    nombre_area = fields.Char(string="Nombre Área",track_visibility='onchange', index=True)
    cod_area = fields.Char(string="Código de Área",track_visibility='onchange', index=True, size=2)

    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)



@api.multi
@api.returns('mail.message', lambda value: value.id)
def message_post(self, **kwargs):
    if self.env.context.get('mark_rfq_as_sent'):
        self.filtered(lambda o: o.state == 'draft').write({'state': 'sent'})
    return super(PurchaseOrder, self.with_context(mail_post_autofollow=True)).message_post(**kwargs)

